<?php

include 'vendor/autoload.php';

//progress bar init
use Dariuszp\CliProgressBar;
$bar = new CliProgressBar(4);
$bar->display();

// youtube parser
$name = $argv[1];
$title = str_replace(' ','+',$name);
$query = "https://www.youtube.com/results?search_query=$title";
// echo "$query";
$html = file_get_contents($query);
$dom = new DOMDocument;
@$dom->loadHTML($html);
$links = $dom->getElementsByTagName('a');
foreach ($links as $link){
    if (strpos($link->getAttribute('href'), "/watch?v=") !== false) {
    	// echo $link->getAttribute("href"),"\n";
		$youtubeVideoLink = $link->getAttribute("href");
		//commentare il break per avere elenco di tutti i video
		break;
    }
}
$bar->progress();

//gathering video meta
$title = "";
$videoid = str_replace("/watch?v=","",$youtubeVideoLink);
$html = 'https://www.googleapis.com/youtube/v3/videos?id='.$videoid.'&key=AIzaSyDIsnMbe5BCbgbsilOZBUbTpCWmMHhTe9w&part=snippet';
    $response = file_get_contents($html);
    $decoded = json_decode($response, true);
    foreach ($decoded['items'] as $items) {
         $title= $items['snippet']['title'];
    }
$bar->progress();

//video download
// echo "\nDownloading $title...";
$yt = new YouTubeDownloader();
$links = $yt->getDownloadLinks($youtubeVideoLink);
//22 mp4 HD circa 40MB
//18 mp4 circa 18MB
$videoLink = $links[18]["url"];
file_put_contents("video.mp4", fopen($videoLink, 'r'));
$bar->progress();

//mp4 to mp3
exec("ffmpeg -i video.mp4 -f mp3 -ab 192000 -vn \"$title.mp3\" -loglevel quiet");
$bar->progress();

//cleaning
exec("rm video.mp4");
$bar->progress();

$bar->display();
$bar->end();
echo "Download completed. Have a good day";
